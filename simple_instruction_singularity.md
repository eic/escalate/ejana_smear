# Fast simulations for the EIC

We show how to run fast simulations from the command line. In our instructions, we assume that you work on a BNL or JLAB system. 

Alternatively to the Singularity approach we present, you can [use JupyterHub](https://gitlab.com/eic/escalate/workspace/-/blob/master/RemoteWork.md) (and skip the first two steps below). We also provide [instructions using Docker](https://gitlab.com/eic/escalate/smear/-/blob/master/simple_instruction_docker.md) instead of Singularity for work on your local system. 

## 1. Load Singularity
The [Singularity](https://sylabs.io/singularity/) virtualization software is available for Linux amd already installed systemwide on BNL and JLAB systems. The recommended version can be loaded with `module load singularity`. The instructions below are written to work with singularity versions 2.6 and any version in the 3.x series.

For more details, please see our [documentation on using Singularity containers](https://gitlab.com/eic/escalate/workspace/blob/master/Singularity.md). 

## 2. Located the Singularity image
The ESCalate images are deployed on Docker Hub and are pulled from there into Singularity images on the [CernVM-FS](https://cvmfs.readthedocs.io/en/stable/) filesystem accessible on the BNL and JLAB systems with each new version. The Singularity image used below is located at `/cvmfs/eic.opensciencegrid.org/singularity/escalate:1.0.0`.

For more details, please see our [documentation on container versions and the software included in each image](https://gitlab.com/eic/containers#software-version-table). 

## 3. Smear a generator file

### Smear a file in your current directory

To smear a generator file called ```my_file.dat``` located in the current directory of your system: 
```sh
singularity run /cvmfs/eic.opensciencegrid.org/singularity/escalate:1.0.0 smear my_file.dat
```
For tests, you can use an [example file for ep](https://gitlab.com/eic/eic-smear/-/raw/master/tests/ep_hiQ2.20x250.small.txt?inline=false) (Pythia6, 86 MB) or an 
[example file for ed](https://gitlab.com/eic/escalate/workspace/-/blob/master/data/beagle_eD.txt?inline=false) (BeAGLE, 49MB). 

### Smear a file in your specific directory 

To smear a file located at ```/some/dir/my_file.dat```:
```sh
singularity run /cvmfs/eic.opensciencegrid.org/singularity/escalate:1.0.0 smear /some/dir/my_file.dat
```

In this example, we bind ```/some/dir/``` to Docker. The output file will be located in the same directory

### Simplify the Singularity commands
For simplicity, you can set an alias: 
```bash
alias smear='singularity run /cvmfs/eic.opensciencegrid.org/singularity/escalate:1.0.0 smear'
```
Now you can just do:
```bash
smear my_file.dat
```
This will work only for files in the current directory. 

## 4. Working with the wrapper script 

For more details, please see our [full documentation on the smear command](https://gitlab.com/eic/escalate/smear). 

### Select a detector

By default the latest Handbook detector from eic-smear package is used. To change the detector use ```-d <name>```flag: 
```sh
singularity run /cvmfs/eic.opensciencegrid.org/singularity/escalate:1.0.0 smear -d jleic my_file.dat
```
To see all detectors and versions use ```-l```flag. 

### Specify number of events
One can set a number of events by ```-n(--nevents)``` and ```-s(--nskip)``` flags. Flag ```-n``` also support ranges:
```  
-n 100        : process 100 events
-s 100 -n 50  : skip first 100 events, and process 50 events
-s 100        : skip first 100 events and process the rest
```
### Available detector names and versions
| Engine | Name         | Version and Link |
|--------|--------------|------------------|
| ES     | handbook     | [HandBook v1.0.4](https://gitlab.com/eic/escalate/ejana/blob/master/src/plugins/reco/eic_smear/ESDetectorHandBook_v1_0_4.cc) |
| ES     | beast        | [BeAST v1.0.4](https://gitlab.com/eic/escalate/ejana/blob/master/src/plugins/reco/eic_smear/ESDetectorBeAST_v1_0_4.cc) |
| ES     | ephenix      | [ePHENIX v1.0.4](https://gitlab.com/eic/escalate/ejana/blob/master/src/plugins/reco/eic_smear/ESDetectorEPHENIX_v1_0_4.cc) |
| ES     | zeus         | [DetectorZeus v1.0.0](https://gitlab.com/eic/escalate/ejana/blob/master/src/plugins/reco/eic_smear/ESDetectorZeus_v1_0_0.cc) |
| YF     | yfhandbook   | [Handbook v1.0.0](https://gitlab.com/eic/escalate/ejana/blob/master/src/plugins/reco/eic_smear/YFDetectorHandbook_v1_0_0.cc) |
| YF     | jleic        | [Jleic v1.0.2](https://gitlab.com/eic/escalate/ejana/blob/master/src/plugins/reco/eic_smear/YFDetectorJleic_v1_0_2.cc) |
| YF     | jleic-v1.0.1 | [Jleic v1.0.1](https://gitlab.com/eic/escalate/ejana/blob/master/src/plugins/reco/eic_smear/YFDetectorJleic_v1_0_1.cc) |
> Comment: This section needs to be improved with better sames for the smearing engines, currently listed as ES - eic-smear and YF - Yulia Furletova, and the detecot names. 


