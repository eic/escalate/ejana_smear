# Fast simulations for the EIC

We show how to run fast simulations from the command line. In our instructions, we assume that you work on your local system. Alternatively to the Docker and Singularity approaches we present, you can [use JupyterHub](https://gitlab.com/eic/escalate/workspace/-/blob/master/RemoteWork.md) (and skip the first two steps below). 

## Using Docker

### 1. Install Docker
The [Docker](https://www.docker.com) virtualization software is available for Linux, macOS, and Windows. Please follow the instructions to install, configure, and test Docker on your system: 

- **Linux**: [Docker Engine](https://hub.docker.com/search/?type=edition&offering=community) (free in the Community Edition)
- **macOS**: [Docker Desktop](https://www.docker.com/products/docker-desktop) (free)
- **Windows**: [Docker Desktop](https://www.docker.com/products/docker-desktop) (free)

For more details, please see our [documentation on using Docker containers](https://gitlab.com/eic/escalate/workspace/blob/master/Docker.md). 

### 2. Get Docker image
The ESCalate images are deployed on Docker Hub. The latest versions can be obtained via: 
```sh
docker pull electronioncollider/escalate
```
For more details, please see our [documentation on container versions and the software included in each image](https://gitlab.com/eic/containers#software-version-table). 

> Comment: Should we use concrete version numbers instead of latest? 

## Using Singularity

### 1. Install Singularity (alternative to Docker)
The [Singularity](https://sylabs.io/singularity/) virtualization software is available for Linux. Please follow the instructions to install, configure, and test Singularity on your system: 

- **Linux**: [Singularity CE](https://github.com/hpcng/singularity/releases) (free in the Community Edition)

For more details, please see our [documentation on using Singularity containers](https://gitlab.com/eic/escalate/workspace/blob/master/Singularity.md). 

### 2. Get Singularity image
The ESCalate images are deployed on Docker Hub and can be pulled from there by Singularity. The latest versions can be obtained via: 
```sh
singularity pull docker://electronioncollider/escalate:latest
```
This will create a file `escalate_latest.sif` in the current directory. For more details, please see our [documentation on container versions and the software included in each image](https://gitlab.com/eic/containers#software-version-table). 

> Comment: Should we use concrete version numbers instead of latest? 

## 3. Smear a generator file

### Smear a file in your current directory

To smear a generator file called ```my_file.dat``` located in the current directory of your system: 
```sh
docker run -it --rm -v `pwd`:/home/eicuser/workspace electronioncollider/escalate:latest smear my_file.dat
```
or 
```sh
singularity run docker://electronioncollider/escalate:latest smear my_file.dat
```
For tests, you can use [an example file](https://gitlab.com/eic/eic-smear/-/raw/master/tests/ep_hiQ2.20x250.small.txt?inline=false) (86 MB).

### Smear a file in your specific directory 

To smear a file located at ```/some/dir/my_file.dat```:
```sh
docker run -it --rm -v /some/dir/:/home/eicuser/workspace electronioncollider/escalate:latest smear my_file.dat
```
or
```sh
singularity run docker://electronioncollider/escalate:latest smear /some/dir/my_file.dat
```

In this example, we bind ```/some/dir/``` to Docker. The output file will be located in the same directory

### Simplify the Docker or Singularity commands
For simplicity, you can set an alias: 
```bash
alias smear='docker run -it --rm -v `pwd`:/home/eicuser/workspace electronioncollider/escalate:latest smear'
```
or
```bash
alias smear='singularity run docker://electronioncollider/escalate:latest smear'
```

Now you can just do:
```bash
smear beagle_eD.txt
```
This will work only for files in the current directory. 

## 4. Working with the wrapper script 

For more details, please see our [full documentation on the smear command](https://gitlab.com/eic/escalate/smear). 

### Select a detector

By default the latest Handbook detector from eic-smear package is used. To change the detector use ```-d <name>```flag: 
```sh
docker run -it --rm -v /some/dir/:/home/eicuser/workspace electronioncollider/escalate:latest smear -d jleic my_file.dat
```
or
```sh
singularity run docker://electronioncollider/escalate:latest smear -d jleic my_file.dat
```
To see all detectors and versions use ```-l```flag. 

### Specify number of events
One can set a number of events by ```-n(--nevents)``` and ```-s(--nskip)``` flags. Flag ```-n``` also support ranges:
```  
-n 100        : process 100 events
-s 100 -n 50  : skip first 100 events, and process 50 events
-s 100        : skip first 100 events and process the rest
```
### Available detector names and versions
| Engine | Name         | Version and Link |
|--------|--------------|------------------|
| ES     | handbook     | [HandBook v1.0.4](https://gitlab.com/eic/escalate/ejana/blob/master/src/plugins/reco/eic_smear/ESDetectorHandBook_v1_0_4.cc) |
| ES     | beast        | [BeAST v1.0.4](https://gitlab.com/eic/escalate/ejana/blob/master/src/plugins/reco/eic_smear/ESDetectorBeAST_v1_0_4.cc) |
| ES     | ephenix      | [ePHENIX v1.0.4](https://gitlab.com/eic/escalate/ejana/blob/master/src/plugins/reco/eic_smear/ESDetectorEPHENIX_v1_0_4.cc) |
| ES     | zeus         | [DetectorZeus v1.0.0](https://gitlab.com/eic/escalate/ejana/blob/master/src/plugins/reco/eic_smear/ESDetectorZeus_v1_0_0.cc) |
| YF     | yfhandbook   | [Handbook v1.0.0](https://gitlab.com/eic/escalate/ejana/blob/master/src/plugins/reco/eic_smear/YFDetectorHandbook_v1_0_0.cc) |
| YF     | jleic        | [Jleic v1.0.2](https://gitlab.com/eic/escalate/ejana/blob/master/src/plugins/reco/eic_smear/YFDetectorJleic_v1_0_2.cc) |
| YF     | jleic-v1.0.1 | [Jleic v1.0.1](https://gitlab.com/eic/escalate/ejana/blob/master/src/plugins/reco/eic_smear/YFDetectorJleic_v1_0_1.cc) |
> Comment: This section needs to be improved with better sames for the smearing engines, currently listed as ES - eic-smear and YF - Yulia Furletova, and the detecot names. 


