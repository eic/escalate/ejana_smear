import argparse
import unittest
import os
from smear.runner import makeup_output_file_name, parse_arguments, _get_jana_args, process_arguments


class TestParametersParsing(unittest.TestCase):

    def setUp(self) -> None:
        self.this_dir = os.path.dirname(os.path.abspath(__file__))
        self.data_dir = os.path.join(self.this_dir, 'test_data')
        self.beagle_file = os.path.join(self.data_dir, 'beagle_eD.txt')

    def test_makeup_output_file_name(self):
        result = makeup_output_file_name("/some/dir/file.name.lund")
        self.assertEqual(result, 'file.name.root')

    def test_event_ranges(self):
        params = argparse.Namespace()

    def test_wrong_file_name(self):
        result = process_arguments(["non_existent_file.txt"])
        self.assertEqual(result, 1)

    def test_wrong_detector(self):
        result = process_arguments([self.beagle_file, '-d', 'non_existent_detector'])
        self.assertEqual(result, 1)

    def test_many_files(self):
        result = process_arguments([self.beagle_file, self.beagle_file])
        self.assertEqual(result, 1)

    def test_get_jana_args(self):
        params = argparse.Namespace()
        params.nevents = 100
        params.nskip = 20
        params.output = "haha.txt"
        params.threads = 1
        jana_args = _get_jana_args(params)
        self.assertEqual(jana_args["nthreads"], 1)
        self.assertEqual(jana_args["nevents"], 100)
        self.assertEqual(jana_args["nskip"], 20)
        self.assertEqual(jana_args["output"], params.output)


if __name__ == '__main__':
    unittest.main()
